package com.atlassian.bamboo.plugin.buildtimes;

import com.atlassian.bamboo.build.BuildExecutionManager;
import com.atlassian.bamboo.buildqueue.manager.AgentManager;
import com.atlassian.bamboo.resultsummary.BuildResultsSummary;
import com.atlassian.bamboo.v2.build.CurrentlyBuilding;

import java.util.Date;
import java.util.concurrent.TimeUnit;

public class JobTime {
    private long preQueuedDuration; // for jobs in later stages...
    private long jobQueueDuration;
    private long buildingDuration;
    private long vcsUpdatingDuration;

    private long totalDuration;
    private String jobName;
    private String result;
    private Boolean rerun;
    private String agentName;
    private long agentId;

    public JobTime(BuildResultsSummary resultsSummary, Date startTime, AgentManager agentManager, BuildExecutionManager buildExecutionManager) {
        //Agent information
        this.agentName = "Unknown";
        this.agentId = -1;
        try {
            this.agentId = resultsSummary.getBuildAgentId();
            this.agentName = agentManager.getAgent(this.agentId).getName();
        }
        catch (NullPointerException e)
        {
            //Couldn't get the agent id or BuildAgent
            this.agentName = "Unknown";
            this.agentId = -1;
        }

        //Rerun
        this.rerun = resultsSummary.isRebuild();

        //Job Name
        this.jobName = resultsSummary.getImmutablePlan().getBuildName();

        //Build result
        this.result = "current";
        if (resultsSummary.isFinished())
        {
            this.result = "red";
            if(resultsSummary.isSuccessful())
            {
                this.result = "green";
            }
        }
        else if (resultsSummary.isPending())
        {
            this.result = "pending";
        }
        else if (resultsSummary.isQueued())
        {
            this.result = "queued";
        }
        else if (resultsSummary.isNotBuilt())
        {
            this.result = "not built";
        }

        //Timing info
        Date queueTime = resultsSummary.getQueueTime();
        if (queueTime != null) {
            //Hack - if the build was rerun, this can come out negative.
            this.preQueuedDuration = Math.max(TimeUnit.MILLISECONDS.toSeconds(queueTime.getTime() - startTime.getTime()),0);
        }
        else {
            this.preQueuedDuration = 0;
        }

        this.jobQueueDuration = TimeUnit.MILLISECONDS.toSeconds(resultsSummary.getQueueDuration());
        this.vcsUpdatingDuration = TimeUnit.MILLISECONDS.toSeconds(resultsSummary.getVcsUpdateDuration());
        this.buildingDuration = TimeUnit.MILLISECONDS.toSeconds(resultsSummary.getDuration());

        //Hack - bamboo doesn't populate the building duration until the build is finished.
        if(this.result.equals("current"))
        {
            CurrentlyBuilding currentlyBuilding = buildExecutionManager.getCurrentlyBuildingByPlanResultKey(resultsSummary.getPlanResultKey());
            if(currentlyBuilding != null && !currentlyBuilding.isUpdatingVcs())
                this.buildingDuration = Math.max(TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - startTime.getTime()) - this.jobQueueDuration - this.vcsUpdatingDuration - this.preQueuedDuration,0);
        }

        this.totalDuration = this.jobQueueDuration + this.vcsUpdatingDuration + this.buildingDuration + this.preQueuedDuration;

    }
    
    public String getResult() {
        return this.result;
        
    }

    public long getPreQueuedDuration() {
        return preQueuedDuration;
    }

    public long getQueueDuration() {
        return this.jobQueueDuration;
    }

    public long getBuildingDuration() {
        return this.buildingDuration;
    }

    public long getVcsUpdatingDuration() {
        return vcsUpdatingDuration;
    }
    
    public String getJobName() {
        return this.jobName;
    }
    
    public long getTotalDuration() {
        return this.totalDuration;
    }
    
    public String getAgentName() {
        return this.agentName;
    }

    public long getAgentId() {
        return this.agentId;
    }

    public Boolean isRerun()
    {
        return rerun;
    }
}